"""Sklik Python library
Sklik API is a mess, this library will help you deal with it.
"""

from time import sleep
import socket
from logging import getLogger, DEBUG, INFO
from typing import Optional
from xmlrpc.client import ServerProxy, ProtocolError
from datetime import datetime

COOLDOWN_TIME = 5

CAMPAIGN_FIELDS = [
    "actualClicks",
    "adSelection",
    "automaticLocation",
    "budget.colorCodeId",
    "budget.dayBudget",
    "budget.deleted",
    "budget.deleteDate",
    "budget.id",
    "budget.name",
    "context",
    "contextNetwork",
    "createDate",
    "deleted",
    "deleteDate",
    "defaultBudgetId",
    "endDate",
    "exhaustedTotalBudget",
    "fulltext",
    "id",
    "name",
    "paymentMethod",
    "scheduleEnabled",
    "startDate",
    "status",
    "statusId",
    "totalClicks",
    "totalClicksFrom",
    "totalBudgetFrom",
    "type",
    "totalBudget",
    "videoFormat",
]

REPORT_FIELDS = [
    "actualClicks",
    "adSelection",
    "automaticLocation",
    "budget.colorCodeId",
    "budget.dayBudget",
    "budget.deleted",
    "budget.deleteDate",
    "budget.id",
    "budget.name",
    "context",
    "contextNetwork",
    "createDate",
    "deleted",
    "deleteDate",
    "defaultBudgetId",
    "devicesPriceRatio",
    "endDate",
    "excludedSearchServices",
    "excludedUrls",
    "exhaustedTotalBudget",
    "fulltext",
    "id",
    "name",
    "paymentMethod",
    "schedule",
    "scheduleEnabled",
    "startDate",
    "status",
    "totalClicks",
    "totalClicksFrom",
    "totalBudgetFrom",
    "type",
    "totalBudget",
    "videoFormat",
    "avgCpc",
    "avgPos",
    "clickMoney",
    "clicks",
    "conversions",
    "conversionValue",
    "impressionMoney",
    "impressions",
    "totalMoney",
    "transactions",
    "missImpressions",
    "exhaustedBudget",
    "stoppedBySchedule",
    "underForestThreshold",
    "exhaustedBudgetShare",
    "ctr",
    "pno",
    "ish",
    "ishContext",
    "ishSum",
]

ADS_REPORT_FIELDS = [
    "adStatus",
    "adType",
    "clickthruText",
    "clickthruUrl",
    "createDate",
    "creative1",
    "creative2",
    "creative3",
    "deleted",
    "deleteDate",
    "description",
    "description2",
    "finalUrl",
    "headline1",
    "headline2",
    "headline3",
    "id",
    "path1",
    "path2",
    "longLine",
    "shortLine",
    "name",
    "companyName",
    "skipRate",
    "views",
    "viewRate",
    "avgCostPerView",
    "viewershipRate",
    "mobileFinalUrl",
    "impressionTrackingTemplate",
    "premiseId",
    "premiseModeId",
    "premiseMode",
    "sensitivity",
    "status",
    "trackingTemplate",
    "group.id",
    "campaign.id",
    "avgCpc",
    "avgPos",
    "clickMoney",
    "clicks",
    "conversions",
    "conversionValue",
    "impressionMoney",
    "impressions",
    "totalMoney",
    "transactions",
    "missImpressions",
    "underLowerThreshold",
    "exhaustedBudget",
    "stoppedBySchedule",
    "underForestThreshold",
    "exhaustedBudgetShare",
    "ctr",
    "pno",
    "ish",
    "ishContext",
    "ishSum",
]

BANNERS_REPORT_FIELDS = [
    "adStatus",
    "adType",
    "bannerName",
    "createDate",
    "clickthruUrl",
    "deleted",
    "deleteDate",
    "description",
    "mobileFinalUrl",
    "height",
    "id",
    "imageType",
    "imageUrl",
    "width",
    "premiseId",
    "premiseModeId",
    "premiseMode",
    "status",
    "sensitivity",
    "campaign.actualClicks",
    "campaign.createDate",
    "campaign.deleteDate",
    "campaign.deleted",
    "campaign.endDate",
    "campaign.id",
    "campaign.name",
    "campaign.startDate",
    "campaign.totalBudgetFrom",
    "campaign.totalClicksFrom",
    "campaign.totalClicks",
    "campaign.status",
    "group.id",
    "avgCpc",
    "avgPos",
    "clickMoney",
    "clicks",
    "conversions",
    "conversionValue",
    "impressionMoney",
    "impressions",
    "totalMoney",
    "transactions",
    "missImpressions",
    "underLowerThreshold",
    "exhaustedBudget",
    "stoppedBySchedule",
    "underForestThreshold",
    "exhaustedBudgetShare",
    "ctr",
    "pno",
    "ish",
    "ishContext",
    "ishSum",
]

CAMPAIGN_FIELDS_NONSERIALIZABLE = ["budget"]
AD_FIELDS_NONSERIALIZABLE = ["campaign", "group"]


class NotLoggedError(Exception):
    """Raised when not logged but trying to access scopes of api that require authentication
    """


class ApiError(Exception):
    """Raised when XMLRPC api response status is not 200
    """


class Sklik:
    """Sklik Python API Client
    """

    def __init__(self, base_url="https://api.sklik.cz/drak/RPC2", debug=False, timeout: Optional[float] = None):
        """Constructor
        """
        self.debug = debug
        self.session = None
        self.user_id = None
        self.base_url = base_url
        socket.setdefaulttimeout(timeout)
        self.client = ServerProxy(self.base_url, allow_none=True)
        self.logger = getLogger(__name__)
        self.logger.setLevel(DEBUG if self.debug else INFO)

    def login(self, user=None, password=None, token=None):
        """Login using either username/password combination or using token.

        Args:
            user (str, optional): User name, may be None if using token login. Defaults to None.
            password (str, optional): Password, may be None if using token login. Defaults to None.
            token (str, optional): Token, may be None if using username/password login. Defaults to None.
        """
        if token is not None:
            self.logger.info("Logging in using api token")
            response = self.client.client.loginByToken(token)
            self.session = response["session"]
            return True
        if user is None or password is None:
            self.logger.error("One of user/password or token must be set")
            raise ValueError("One of user/password or token must be set")
        # TODO: username/password login
        self.logger.info("Logging in using username and password combination")
        raise NotImplementedError("This is not implemented yet")

    def list_ads(self, offset=0, limit=1000, deleted=False):
        """List ads starting at {offset}, at most {limit} will be downloaded.
        If you to download all ads at once, see {list_all_ads} method instead.
        Returns deleted ads only if {deleted} is True.

        Args:
            offset (int, optional): Starting position to list ads froms. Defaults to 0.
            limit (int, optional): Max number of ads to return. Defaults to 1000.
            deleted (bool, optional): Return only deleted entries. Defaults to False.
        """
        if self.session is None:
            raise NotLoggedError("Not logged in, call .login first")
        self.logger.info("Listing ads betwen %d and %d", offset, offset + limit)
        try:
            response = self.client.ads.list(
                {"session": self.session},  # user
                {"isDeleted": deleted},  # restrictionFilter
                {"offset": offset, "limit": limit},
            )
        except ProtocolError as err:
            if err.errcode == 429:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.list_ads(offset, limit, deleted)
            elif err.errcode == 500:
                self.logger.warning(
                    "Internal server error occurred, will retry in 30 seconds"
                )
                sleep(30)
                return self.list_ads(offset, limit, deleted)
            raise err
        if response["status"] != 200:
            if response["status"] in [429, 500]:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.list_ads(offset, limit, deleted)
            raise ApiError(response["statusMessage"])
        return response["ads"]

    def list_all_ads(self, deleted=False):
        """Iterate through all pages of ads in order to scrape all of them.
        Returns only deleted ads if {deleted}=True

        Args:
            deleted (bool, optional): Return only deleted ads. Defaults to False.
        """
        offset = 0
        ads = []
        while True:
            current = self.list_ads(offset=offset, limit=5000, deleted=deleted)
            if not current:
                break
            ads.extend(current)
            offset += len(current)
        return ads

    def list_banners(self, offset=0, limit=1000, deleted=False):
        """List banners starting at {offset}, at most {limit} will be downloaded.
        If you to download all banners at once, see {list_all_banners} method instead.
        Returns deleted banners only if {deleted} is True.

        Args:
            offset (int, optional): Starting position to list banners froms. Defaults to 0.
            limit (int, optional): Max number of banners to return. Defaults to 1000.
            deleted (bool, optional): Return only deleted entries. Defaults to False.
        """
        if self.session is None:
            raise NotLoggedError("Not logged in, call .login first")
        self.logger.info("Listing banners betwen %d and %d", offset, offset + limit)
        try:
            response = self.client.banners.list(
                {"session": self.session},  # user
                {"isDeleted": deleted},  # restrictionFilter
                {"offset": offset, "limit": limit},
            )
        except ProtocolError as err:
            if err.errcode == 429:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.list_banners(offset, limit, deleted)
            elif err.errcode == 500:
                self.logger.warning(
                    "Internal server error occurred, will retry in 30 seconds"
                )
                sleep(30)
                return self.list_banners(offset, limit, deleted)
            raise err
        if response["status"] != 200:
            if response["status"] in [429, 500]:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.list_banners(offset, limit, deleted)
            raise ApiError(response["statusMessage"])
        return response["banners"]

    def list_all_banners(self, deleted=False):
        """Iterate through all pages of banners in order to scrape all of them.
        Returns only deleted banners if {deleted}=True

        Args:
            deleted (bool, optional): Return only deleted banners. Defaults to False.
        """
        offset = 0
        banners = []
        while True:
            current = self.list_banners(offset=offset, limit=5000, deleted=deleted)
            if not current:
                break
            banners.extend(current)
            offset += len(current)
        return banners

    def list_campaigns(self, offset=0, limit=1000, deleted=False):
        """List campaigns starting at {offset}, at most {limit} will be downloaded.
        If you need to download all campaigns at once, see {list_all_campaigns} method instead.
        Returns deleted campaigns only if {deleted} is True

        Args:
            offset (int, optional): Starting position to list campaigns from. Defaults to 0.
            limit (int, optional): Max number of campaigns to return. Defaults to 1000.
            deleted (bool, optional): Return only deleted entries. Defaults to False.
        """
        if self.session is None:
            raise NotLoggedError("Not logged in, call .login first")
        self.logger.debug("Listing campaigns between %d and %d", offset, offset + limit)
        try:
            resp = self.client.campaigns.list(
                {"session": self.session},  # user
                {"isDeleted": deleted},  # restrictionFilter
                {"offset": offset, "limit": limit, "displayColumns": CAMPAIGN_FIELDS},
            )
        except ProtocolError as err:
            if err.errcode == 429:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.list_campaigns(offset, limit, deleted)
            elif err.errcode == 500:
                self.logger.warning(
                    "Internal server error occurred, will retry in 30 seconds"
                )
                sleep(30)
                return self.list_campaigns(offset, limit, deleted)
            raise err
        if resp["status"] != 200:
            if resp["status"] in [429, 500]:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.list_campaigns(offset, limit, deleted)
            raise ApiError(resp["statusMessage"])
        return resp["campaigns"]

    def list_all_campaigns(self, deleted=False):
        """Iterate through all pages of campaigns in order to scrape all of them.
        Returns only deleted campaigns if {deleted}=True

        Args:
            deleted (bool, optional): Return only deleted campaigns. Defaults to False.
        """
        offset = 0
        campaigns = []
        while True:
            current = self.list_campaigns(offset=offset, limit=5000, deleted=deleted)
            if not current:
                break
            campaigns.extend(current)
            offset += len(current)
        return campaigns

    def create_report(self, start_date, end_date=datetime.now()):
        """Create report of all campaigns with specified start_date and end_date. Returns report id (str)

        Args:
            start_date (datetime): Start date to create report since
            end_date (datetime, optional): End date of the report. Defaults to today.
        """
        if self.session is None:
            raise NotLoggedError("Not logged in, call .login first")
        self.logger.info("Creating report for specified date range")
        try:
            response = self.client.campaigns.createReport(
                {"session": self.session},
                {"dateFrom": start_date, "dateTo": end_date},
                {},
            )
        except ProtocolError as err:
            if err.errcode == 429:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.create_report(start_date, end_date)
            elif err.errcode == 500:
                self.logger.warning(
                    "Internal server error occurred, will retry in 30 seconds"
                )
                sleep(30)
                return self.create_report(start_date, end_date)
            raise err
        if response["status"] != 200:
            if response["status"] in [429, 500]:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.create_report(start_date, end_date)
            raise Exception(response)
        self.logger.info("Created report id %s", response["reportId"])
        return response["reportId"]

    def read_report(self, report_id):
        """Iterates through report results and returns them all

        Args:
            report_id (str): Report id to be returned
        """
        if self.session is None:
            raise NotLoggedError("Not logged in, call .login first")
        self.logger.info("Reading report id %s", report_id)
        offset = 0
        limit = 5000
        reports = []
        while True:
            try:
                response = self.client.campaigns.readReport(
                    {"session": self.session},
                    report_id,
                    {"offset": offset, "limit": limit, "displayColumns": REPORT_FIELDS},
                )
            except ProtocolError as err:
                if err.errcode == 429:
                    self.logger.warning(
                        "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                    )
                    sleep(COOLDOWN_TIME)
                    continue
                elif err.errcode == 500:
                    self.logger.warning(
                        "Internal server error occurred, will retry in 30 seconds"
                    )
                    sleep(30)
                    continue
                raise err
            if response["status"] != 200:
                if response["status"] in [429, 500]:
                    self.logger.warning(
                        "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                    )
                    sleep(COOLDOWN_TIME)
                    continue
                raise Exception(response)
            if not response["report"]:
                break
            reports.extend(response["report"])
            offset += len(response["report"])
        self.logger.info("Returned %d stats", len(reports))
        return reports

    def create_ads_report(self, start_date, end_date=datetime.now()):
        """Create report of all ads with specified start_date and end_date. Returns report id (str)

        Args:
            start_date (datetime): Start date to create report since
            end_date (datetime, optional): End date of the report. Defaults to today.
        """
        if self.session is None:
            raise NotLoggedError("Not logged in, call .login first")
        self.logger.info("Creating report for specified date range")
        try:
            response = self.client.ads.createReport(
                {"session": self.session},
                {"dateFrom": start_date, "dateTo": end_date},
                {"statGranularity": "daily"},
            )
        except ProtocolError as err:
            if err.errcode == 429:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.create_ads_report(start_date, end_date)
            elif err.errcode == 500:
                self.logger.warning(
                    "Internal server error occurred, will retry in 30 seconds"
                )
                sleep(30)
                return self.create_ads_report(start_date, end_date)
            raise err
        if response["status"] != 200:
            if response["status"] in [429, 500]:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.create_ads_report(start_date, end_date)
            raise Exception(response)
        self.logger.info("Created report id %s", response["reportId"])
        return response["reportId"]

    def read_ads_report(self, report_id):
        """Iterates through ads report results and returns them all

        Args:
            report_id (str): Report id to be returned
        """
        if self.session is None:
            raise NotLoggedError("Not logged in, call .login first")
        self.logger.info("Reading ads report id %s", report_id)
        offset = 0
        limit = 1000
        reports = []
        while True:
            try:
                response = self.client.ads.readReport(
                    {"session": self.session},
                    report_id,
                    {
                        "offset": offset,
                        "limit": limit,
                        "displayColumns": ADS_REPORT_FIELDS,
                    },
                )
            except ProtocolError as err:
                if err.errcode == 429:
                    self.logger.warning(
                        "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                    )
                    sleep(COOLDOWN_TIME)
                    continue
                elif err.errcode == 500:
                    self.logger.warning(
                        "Internal server error occurred, will retry in 30 seconds"
                    )
                    sleep(30)
                    continue
                raise err
            if response["status"] != 200:
                if response["status"] in [429, 500]:
                    self.logger.warning(
                        "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                    )
                    sleep(COOLDOWN_TIME)
                    continue
                raise Exception(response)
            if not response["report"]:
                break
            reports.extend(response["report"])
            offset += len(response["report"])
        self.logger.info("Returned %d stats", len(reports))
        return reports

    def create_banners_report(self, start_date, end_date=datetime.now()):
        """Create report of all banners with specified start_date and end_date. Returns report id (str)

        Args:
            start_date (datetime): Start date to create report since
            end_date (datetime, optional): End date of the report. Defaults to today.
        """
        if self.session is None:
            raise NotLoggedError("Not logged in, call .login first")
        self.logger.info("Creating report for specified date range")
        try:
            response = self.client.banners.createReport(
                {"session": self.session},
                {"dateFrom": start_date, "dateTo": end_date},
                {"statGranularity": "daily"},
            )
        except ProtocolError as err:
            if err.errcode == 429:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.create_banners_report(start_date, end_date)
            elif err.errcode == 500:
                self.logger.warning(
                    "Internal server error occurred, will retry in 30 seconds"
                )
                sleep(30)
                return self.create_banners_report(start_date, end_date)
            raise err
        if response["status"] != 200:
            if response["status"] in [429, 500]:
                self.logger.warning(
                    "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                )
                sleep(COOLDOWN_TIME)
                return self.create_banners_report(start_date, end_date)
            raise Exception(response)
        self.logger.info("Created report id %s", response["reportId"])
        return response["reportId"]

    def read_banners_report(self, report_id):
        """Iterates through banners report results and returns them all

        Args:
            report_id (str): Report id to be returned
        """
        if self.session is None:
            raise NotLoggedError("Not logged in, call .login first")
        self.logger.info("Reading banners report id %s", report_id)
        offset = 0
        limit = 5000
        reports = []
        while True:
            try:
                response = self.client.banners.readReport(
                    {"session": self.session},
                    report_id,
                    {
                        "offset": offset,
                        "limit": limit,
                        "displayColumns": BANNERS_REPORT_FIELDS,
                    },
                )
            except ProtocolError as err:
                if err.errcode == 429:
                    self.logger.warning(
                        "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                    )
                    sleep(COOLDOWN_TIME)
                    continue
                elif err.errcode == 500:
                    self.logger.warning(
                        "Internal server error occurred, will retry in 30 seconds"
                    )
                    sleep(30)
                    continue
                raise err
            if response["status"] != 200:
                if response["status"] in [429, 500]:
                    self.logger.warning(
                        "API Limit reached, cooling down for %d seconds", COOLDOWN_TIME
                    )
                    sleep(COOLDOWN_TIME)
                    continue
                raise Exception(response)
            if not response["report"]:
                break
            reports.extend(response["report"])
            offset += len(response["report"])
        self.logger.info("Returned %d stats", len(reports))
        return reports
