"""Sklik extractor
"""

import os
import json
import csv
import sys
from datetime import datetime, timedelta
from logging import getLogger, DEBUG, INFO, basicConfig
from sklik import Sklik, CAMPAIGN_FIELDS_NONSERIALIZABLE, AD_FIELDS_NONSERIALIZABLE

DATE_FORMAT = "%Y-%m-%d"

def json_serialize(data):
    """Serialize non-serializable data

    Args:
        data (any): Data to be returned as string
    """
    if isinstance(data, datetime):
        return data.isoformat()

def scrape_report(endpoint="campaign", conf={}, logger=None, s=None):
    """Possible values for endpoint: campaign, ads, banners
    """
    if "interval" in conf and conf["interval"]:
        logger.info("Interval is set, start and end dates will be ignored")
        start_date = (datetime.now() + timedelta(days=conf["interval"]))
        logger.info("Interval start date: %s", start_date.strftime(DATE_FORMAT))
        end_date = datetime.now()
    else:
        if "start_date" in conf and conf["start_date"]:
            start_date = datetime.strptime(conf["start_date"], DATE_FORMAT)
        else:
            raise Exception("No start_date was given for {}.stats".format(endpoint))
        if "end_date" in conf and conf["end_date"]:
            end_date = datetime.strptime(conf["end_date"], DATE_FORMAT)
        else:
            end_date = datetime.now()
        logger.info("Fixed dates interval: %s -> %s", start_date.strftime(DATE_FORMAT), end_date.strftime(DATE_FORMAT))
    stats = []
    for days in range(0, (end_date - start_date).days):
        interval_lower = (start_date+timedelta(days=days))
        #interval_upper = (interval_lower + timedelta(days=1))
        interval_upper = interval_lower
        logger.info("Creating report for %s.stats interval %s -> %s", endpoint, interval_lower.strftime(DATE_FORMAT), interval_upper.strftime(DATE_FORMAT))
        if endpoint == "campaign":
            report_id = s.create_report(interval_lower, interval_upper)
            stats.extend(s.read_report(report_id))
        elif endpoint == "ads":
            report_id = s.create_ads_report(interval_lower, interval_upper)
            stats.extend(s.read_ads_report(report_id))
        elif endpoint == "banners":
            report_id = s.create_banners_report(interval_lower, interval_upper)
            stats.extend(s.read_banners_report(report_id))
    stat_budget = []
    stats_cleaned = []
    for stat in stats:
        sid = stat["id"]
        stat_date = stat["stats"][0]["date"]
        if endpoint == "campaign":
            stat_budget.append({"stat_date": stat_date, "stat_id": sid, **stat["budget"]})
        stats_inner = stat["stats"]
        for current in stats_inner:
            current.update({k: v for k, v in stat.items() if k not in [*CAMPAIGN_FIELDS_NONSERIALIZABLE, "stats"]})
            if endpoint in ["ads", "banners"]:
                current["campaign_id"] = stat["campaign"]["id"]
                current["group_id"] = stat["group"]["id"]
            if "campaign" in current:
                del current["campaign"]
            stats_cleaned.append(current)
    if stats_cleaned:
        with open("/data/out/tables/{}_stats.csv".format(endpoint), "w", encoding="utf-8") as fid:
            writer = csv.DictWriter(fid, fieldnames=stats_cleaned[0].keys(), dialect=csv.unix_dialect)
            writer.writeheader()
            writer.writerows([{k: v for k, v in row.items() if k in writer.fieldnames} for row in stats_cleaned])
    if stat_budget:
        with open("/data/out/tables/{}_stats_budget.csv".format(endpoint), "w", encoding="utf-8") as fid:
            writer = csv.DictWriter(fid, fieldnames=stat_budget[0].keys(), dialect=csv.unix_dialect)
            writer.writeheader()
            writer.writerows(stat_budget)

def main(conf_path="config.json", debug=True):
    basicConfig(level=DEBUG if debug else INFO, format="[{asctime}] {levelname:8} {name:>8}:{lineno:03} - {message}", style="{")
    logger = getLogger()
    logger.setLevel(DEBUG if debug else INFO)
    with open(conf_path) as fid:
        conf = json.load(fid)
    timeout = conf.get("timeout", None)
    s = Sklik(timeout=timeout)
    s.login(token=conf["token"])
    if "campaigns" in conf["endpoints"]:
        campaigns = s.list_all_campaigns()
        campaign_budget = []
        for campaign in campaigns:
            cid = campaign["id"]
            campaign_budget.append({"campaign_id": cid, **campaign["budget"]})
        if campaigns:
            campaigns_cleaned = [{k: v for k, v in c.items() if k not in CAMPAIGN_FIELDS_NONSERIALIZABLE} for c in campaigns]
            with open("/data/out/tables/campaign.csv", "w", encoding="utf-8") as fid:
                writer = csv.DictWriter(fid, fieldnames=campaigns_cleaned[0].keys(), dialect=csv.unix_dialect)
                writer.writeheader()
                writer.writerows(campaigns_cleaned)
        if campaign_budget:
            with open("/data/out/tables/campaign_budget.csv", "w", encoding="utf-8") as fid:
                writer = csv.DictWriter(fid, fieldnames=campaign_budget[0].keys(), dialect=csv.unix_dialect)
                writer.writeheader()
                writer.writerows(campaign_budget)
    if "campaigns.stats" in conf["endpoints"]:
        scrape_report("campaign", conf, logger, s)
    if "ads.stats" in conf["endpoints"]:
        scrape_report("ads", conf, logger, s)
    if "banners.stats" in conf["endpoints"]:
        scrape_report("banners", conf, logger, s)
    if "ads" in conf["endpoints"]:
        ads = s.list_all_ads()
        ads_cleaned = []
        for ad in ads:
            current = {k: v for k, v in ad.items() if k not in AD_FIELDS_NONSERIALIZABLE}
            current["group_id"] = ad["group"]["id"]
            current["group_name"] = ad["group"]["name"]
            current["campaign_id"] = ad["campaign"]["id"]
            current["campaignname"] = ad["campaign"]["name"]
            ads_cleaned.append(current)
        if ads_cleaned:
            with open("/data/out/tables/ads.csv", "w", encoding="utf-8") as fid:
                writer = csv.DictWriter(fid, fieldnames=ads_cleaned[0].keys(), dialect=csv.unix_dialect)
                writer.writeheader()
                writer.writerows(ads_cleaned)
    if "banners" in conf["endpoints"]:
        banners = s.list_all_banners()
        banners_cleaned = []
        for banner in banners:
            current = {k: v for k, v in banner.items() if k not in AD_FIELDS_NONSERIALIZABLE}
            current["group_id"] = banner["group"]["id"]
            current["group_name"] = banner["group"]["name"]
            current["campaign_id"] = banner["campaign"]["id"]
            current["campaignname"] = banner["campaign"]["name"]
            banners_cleaned.append(current)
        if banners_cleaned:
            with open("/data/out/tables/banners.csv", "w", encoding="utf-8") as fid:
                writer = csv.DictWriter(fid, fieldnames=banners_cleaned[0].keys(), dialect=csv.unix_dialect)
                writer.writeheader()
                writer.writerows(banners_cleaned)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        conf_path = sys.argv[1]
    else:
        conf_path = "config.json"
    main(conf_path=conf_path)
