FROM python:3.7-slim

ADD code /code
WORKDIR /code

ENTRYPOINT [ "python", "-u", "main.py", "/config/config.json" ]
