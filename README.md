# ex-sklik

Sklik extractor using [XMLRPC api](https://api.sklik.cz/drak/index.html).

Supported endpoints:

- `campaigns`
- `campaigns.stats`
- `ads`
- `banners`
- `ads.stats`
- `banners.stats`

## config.json

**NOTE** either `token` or `user` and `password` must be non-null.

```json
{
  "token": null,
  "user": null,
  "password": null,
  "endpoints": [
    "campaigns",
    "campaigns.stats",
    "ads",
    "banners",
    "ads.stats",
    "banners.stats"
  ],
  "start_date": "2020-01-01",
  "end_date": null,
  "interval": -7,
  "timeout": null | 120
}
```

### notes

- `start_date` and `end_date` and `interval` are used only for `*.stats`
- `end_date` may be `null` or omitted (defaults to `today`)
- `start_date` and `date_date` are expected to be in format `YYYY-MM-DD` if not omitted or `null`
- `interval` is number of days to add to current day (`-7` = download last 7 days, excluding today)
- `timeout` is a float specifying timeout for each request (or `null` meaning there is no timeout)
